# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Assignment

In the public directory you will find two images of forms, one called login.png and the other called business-login.png.

The assets needed for login page are login-image.png and bixihub-logo.png. The asset needed for business-login is business-login-image.png.

You might notice that the asset images are not completely corresponding to the pictures of the page, don't worry about it! Please use the images as per instructions.

#### Requirements: 
* You must implement login.png and business-login.png webpages.
* The login page should be on the root route. The business-login page should be on /business-login route.
* When user click on submit, the result should be logged in the console and all fields should be cleared.

#### Bonus:
* Wherever there is an eye icon, the visibility of the input should be changed.
* Make one of the pages responsive.

You may use any tools/libraries that you like, but you have to use react and typescript.